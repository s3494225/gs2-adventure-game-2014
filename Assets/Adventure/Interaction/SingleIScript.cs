using UnityEngine;
using System;
using System.Collections;

/// <summary>
/// Base script for all "iScripts" that handle when player
/// interacts with this object only.
/// </summary>
public abstract class SingleIScript : InteractionScript
{
	public override void OnInteract(bool success)
	{
		Debug.Log("Running single iScript " + this + 
			" and success=" +  success);
		
		if (success)
			OnSuccess();
		else
			OnOutOfRange();
	}
	
	/// <summary>
	/// What to do when executed successfully.
	/// </summary>
	protected abstract void OnSuccess();
	
	/// <summary>
	/// What to do when player wants to execute but is out of range.
	/// </summary>
	protected abstract void OnOutOfRange();
}

