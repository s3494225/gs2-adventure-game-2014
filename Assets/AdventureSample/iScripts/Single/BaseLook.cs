using UnityEngine;
using System.Collections;

public class BaseLook : SingleIScript
{
	public string[] dialogue;
	
	protected override void OnSuccess()
	{
		Helpers.DialogueManager.SpeakNow(dialogue);
	}
	
	protected override void OnOutOfRange()
	{
	}
}

